#include <stdio.h>
#include <stdbool.h>

#define NoGoodMove   -1 //константа для ситуации «плохого хода»
int iniMatches=0; //начальное колличество спичек, не задано
int MaxTake =1; //начальное кол-во спичек, которые можно взять

typedef enum { Human, Computer } playerT; //различаем ходы человека и компьютера


static void winnerIs(int nMatches, playerT whoseTurn); //прототипы функции
static int userMove(int nMatches);
static bool isthisLegal(int nTaken, int nMatches);
static int aiMove(int nMatches);
static int goodMove(int nMatches);
static bool isbadPosition(int nMatches);


int main()
{
    int nMatches, nTaken;
    playerT whoseTurn;
    printf("Привет, добро пожаловать в игру Спички Бергсона.\n");
    printf("В этой игре каждый игрок поочередно берет спички из общей кучи\n");
    printf("Не меньше одной и не больше, чем взял другой игрок");
    printf("Тот, кто возьмет последнюю - побеждает");
    printf("А сейчас выберете начальное колличество спичек\n" );
    scanf("%i",&iniMatches);
    nMatches = iniMatches;
    whoseTurn = Human;
    while (nMatches > 1) {
        printf("Сейчас у нас %d спичек.\n", nMatches);
        switch (whoseTurn) {
          case Human:
            nTaken = userMove(nMatches);
            MaxTake=2*nTaken;
            whoseTurn = Computer;
            break;
          case Computer:
           nTaken = aiMove(nMatches);
            printf("Я возьму %d.\n", nTaken);
            MaxTake=2*nTaken;
            whoseTurn = Human;
            break;
        }
        nMatches -= nTaken;
    }
    winnerIs(nMatches, whoseTurn);
}

static void winnerIs(int nMatches, playerT whoseTurn)
{
    if (nMatches == 0) {
        switch (whoseTurn) {
          printf(«Спичек не осталось. Победа»)
        }
    } else {
        printf("Осталась лишь одна спичка.\n");
        switch (whoseTurn) {
          case Human:    printf("Я проиграл.\n"); break;
          case Computer: printf("Я победил.\n"); break;
        }
    }
}

static int userMove(int nMatches)
{
    int nTaken, limit;

    while (true) {
        printf("Сколько спичек надо взять?");
        scanf("%i",&nTaken);
        if (isthisLegal(nTaken, nMatches)) break;
        limit = (nMatches < MaxTake) ? nMatches : MaxTake;
        printf("Это неверный ход! Возьмите");
        printf(" между 1 и %d спичками\n", limit);
        printf("Осталось %d спичек.\n", nMatches);
    }
    return (nTaken);
}
static bool isthisLegal(int nTaken, int nMatches)
{
    return (nTaken > 0 && nTaken <= MaxTake && nTaken <= nMatches);
}
static int aiMove(int nMatches)
{
    int nTaken;

    nTaken = goodMove(nMatches);
    if (nTaken == NoGoodMove) nTaken = 1;
    return (nTaken);
}

static int goodMove(int nMatches)
{
    int nTaken;

    for (nTaken = 1; nTaken <= MaxTake; nTaken++) {
        if (isbadPosition(nMatches - nTaken)) return (nTaken);
    }
    return (NoGoodMove);
}

static bool isbadPosition(int nMatches)
{
    if (((nMatches%2==1)&&!(nMatches<=3))||(nMatches<=0)) return (true); 
    return (goodMove(nMatches) == NoGoodMove);
}